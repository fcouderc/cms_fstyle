import matplotlib.pyplot as plt

import cms_fstyle as plotting
from cms_fstyle.stats import FCov2D, FCovND, disp_cov, mic_cor
import cms_fstyle.stats.rand_var as gene

import numpy as np


# ----------------------------------------
# -- Plotting example
# ----------------------------------------
def example_plot():
    plt.figure(figsize=[8, 6])

    data1 = np.random.normal(0, 1, 1000)
    data2 = np.random.normal(1, 2, 1000)
    bins = np.linspace(-5, 5, 51)
    hist1, _ = np.histogram(data1, bins=bins)
    hist2, _ = np.histogram(data2, bins=bins)

    # -- histogram plotting
    plotting.draw(x=bins, y=hist1, legend='hist 1', option='fill')
    plotting.draw(x=bins, y=hist2, yerr=np.sqrt(hist2), legend='hist 2', option='E')

    # -- graph style
    x = np.linspace(-5, 5, 20)
    y = np.random.randint(0, 50, 20)
    yerr = np.sqrt(y)
    plotting.draw(x=x, y=y, yerr=yerr, legend='graph', option='E1')

    plotting.polish_axis(x_title='x axis (A.U.)', y_title="y axis (A.U.)", x_range=(-6, 6), y_range=(0,),
                         leg_title='sample plot', cms=True)


# ----------------------------------------
# -- Test the class FCov2D
# ----------------------------------------
def example_fcov2d(n_evt=100000):
    """
    Test function for the cov2d class
    :param n_evt:
    :return:
    """

    fig, axes = plt.subplots(2, 4, figsize=[12, 8], sharex=True, sharey=True)
    x_unif = gene.x_uniform(n_evt, a=4)
    x_norm = gene.x_normal(n_evt)

    samples = [[(x_norm, gene.y_parab(x_norm, s_y=0.1)), (x_unif, gene.y_circ(x_unif, s_y=0.05)),
                (x_norm, gene.y_parab2(x_norm, s_y=0.1)), (x_unif, gene.y_cos(x_unif, s_y=0.10))],
               [(x_norm, gene.y_parab(x_norm, s_y=0.8)), (x_unif, gene.y_circ(x_unif, s_y=0.20)),
                (x_norm, gene.y_parab2(x_norm, s_y=0.8)), (x_unif, gene.y_cos(x_unif, s_y=1.0))]]

    covar = None
    for i in range(2):
        for j in range(4):
            x, y = samples[i][j]
            covar = FCov2D(x, y)
            covar.plot(ax=axes[i, j])
            l_cor = covar.corcoef()    # linear correlation
            d_cor = covar.d_corcoef()  # distance correlation
            mi_cor = covar.mutual_information()  # mutual information
            t_str = f'L-corr = {l_cor:>3.2f}\nD-corr = {d_cor:>3.2f}\nMI = {mi_cor:>3.2f}'
            tt = axes[i, j].text(-2, -4., t_str, fontsize=15, color='k')
            tt.set_bbox({'facecolor': 'w', 'alpha': 0.8, 'edgecolor': 'none'})
            axes[i, j].set_ylim(-5, 5)
            axes[i, j].set_xlim(-4, 4)

    with np.printoptions(precision=3):
        print('Pearson covariance matrix')
        print(covar.cov())
        print('Distance covariance matrix')
        print(covar.d_cov())


# ----------------------------------------
# -- Test the class FCovND
# ----------------------------------------
def example_fcovnd(n_evt=100000):
    """
    Test function for the cov2d class
    :param n_evt:
    :return:
    """
    x = gene.x_uniform(n_evt, 3)
    y = gene.y_circ(x, s_y=0.10)
    z = gene.y_cos(y, s_y=0.4)

    fig1, axes = plt.subplots(2, 2, figsize=[8, 8])
    covar = FCovND(np.array([x, y, z]).T)

    for i in range(3):
        for j in range(i+1, 3):
            covar.plot_2d(i, j, ax=axes[i, j-1])
            plotting.polish_axis(axes[i, j-1], x_title=covar.names[i], y_title=covar.names[j])

    fig2, axes = plt.subplots(1, 3, figsize=[9, 3])
    disp_cov(covar.cor(), ax=axes[0], vmin=0, vmax=0.5)
    disp_cov(covar.d_cor(), ax=axes[1], vmin=0, vmax=0.5)
    disp_cov(mic_cor(np.array([x, y, z]).T, qt=True), ax=axes[2], vmin=0, vmax=0.5, colorbar=True)

    axes[0].set_title('linear corr.')
    axes[1].set_title('distance corr.')
    axes[2].set_title('MIC')
    return fig1, fig2


if __name__ == '__main__':
    # -- plotting  example
    example_plot()
    plt.gcf().savefig('cms_fstyle/resources/sample.png')

    # -- 2D covariances example
    example_fcov2d()
    plt.gcf().savefig('cms_fstyle/resources/covariance.png')

    # -- ND covariances example
    figs = example_fcovnd()
    figs[0].savefig('cms_fstyle/resources/covariance_proj.png')
    figs[1].savefig('cms_fstyle/resources/correlation.png')

    plotting.show()
